import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import MenuItem from '@material-ui/core/MenuItem';
import LoginContext from '../contexts/LoginContext';



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));



export default function ButtonAppBar() {
  const classes = useStyles();
  const [loggedIn, setLoggedIn] = useState(true);
  const status = useContext(LoginContext)

  return (
    <div className={classes.root}>
      <AppBar position="static" >
        
          <Toolbar>
            <Typography className={classes.AppBar }>
              
            </Typography>
            <Typography className={classes.AppBar }>
              <MenuItem component={Link} to="/">Home</MenuItem>
            </Typography>
            <Typography className={classes.AppBar }>
              <MenuItem component={Link} to="/about">About</MenuItem>
            </Typography>
            <Typography className={classes.AppBar }>
              <MenuItem component={Link} to="/signup">Signup</MenuItem>
            </Typography>
            <Typography className={classes.AppBar }>
              <MenuItem component={Link} to="/promotion">Promote</MenuItem>
            </Typography>
            
            
            
          </Toolbar>
      </AppBar>
    </div>
  );
}

