import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import {Link} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUp() {
  const classes = useStyles();

  const [saleItem, setSaleItem] = useState({});
  const [saleItemDTO, setSaleItemDTO] = useState({itemName: "", city: "", price: 0.00, description: "" });
  const [id, setid] = useState(0);

const handleId = (event) => {
    setid(event.target.value)
}


  const handleChange = (event) => {
    setSaleItemDTO({
        ...saleItemDTO,
        [event.target.name]: event.target.value,
    });
};

const handlePromotion = (event) => {
    event.preventDefault();

    axios.post("http://localhost:8080/customers/" + id, saleItemDTO)
    .then((response) => setSaleItem(response.data))

};
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
         Promote your product for sale on Web Market. <br />
        </Typography>
        <form onSubmit={(e) => handlePromotion(e)} className={classes.form} Validate>
          <Grid container spacing={2}>
            <Grid item xs={12} >
              <TextField autoComplete="id" id="id" variant="outlined" required 
              label="Id" autoFocus onChange={handleId} />
            </Grid> <Grid item xs={12} >
              <TextField autoComplete="iname" name="itemName" variant="outlined" required fullWidth
              id="itemName" label="Item Name" autoFocus onChange={(e) => handleChange(e)}/>
            </Grid>
            <Grid item xs={12} >
              <TextField autoComplete="city" name="city" variant="outlined" required fullWidth
              id="city" label="City" autoFocus onChange={(e) => handleChange(e)}/>
            </Grid>
            <Grid item xs={12} >
              <TextField autoComplete="price" name="price"variant="outlined" required fullWidth 
              id="price" label="Price" autoFocus onChange={(e) => handleChange(e)} />
            </Grid>
            <Grid item xs={12}>
              <TextField autoComplete="description" name="description" variant="outlined" required fullWidth 
              id="description" label="Description" autoFocus onChange={(e) => handleChange(e)} />
            </Grid>
          </Grid>
          <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit} >
            Submit
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="./SignUp" variant="body2">
                Don't you have an account? Sign up
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}