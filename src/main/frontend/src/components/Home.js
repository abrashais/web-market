import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import {Link} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';


const handleClick = (id) => {

  try {
    axios.get(`http://localhost:8080/promotions/customerId/${id}`)
      .then((response) => {
        console.log(response.data);
        alert("Advertiser contact number: " + response.data);
      })
  } catch (Error) {
    alert("Cannot find advertiser.")
  };
}

const ReturnSaleItems = (props) => {
  return(
      <ul>
          {props.saleItems.map((saleItem, i) => {
              return (
                  <li key={i}>
                    <Link to="#" onClick={() => handleClick(saleItem.id)}>  {saleItem.itemName} <br /> 
                        {saleItem.description}  <br/> {saleItem.city}</Link>
                  </li>
              );
          })}
      </ul>
  )
} 

const Home = () => {
    const [name, setName] = useState("");
    const [saleItems, setSaleItems] = useState([]);

    const useStyles = makeStyles((theme) => ({
        root: {
          '& > *': {
            margin: theme.spacing(1),
            width: '50%',
            marginLeft: 100
          },
        },
      }));

  const handleSearch = (event) => {
    event.preventDefault();
    try {
      axios.get(`http://localhost:8080/promotions/search/${name}`)
        .then((response) => {
          console.log(response.data);
          setSaleItems(response.data);

        })
    } catch (err) {
      if(setSaleItems === null) {
        alert("Search attempt returned 0 result.")
      }
    }
  }

  const classes = useStyles();

  return (
      <div>
    <Container component="main" maxWidth="xs" md="1">
    {/* <CssBaseline /> */}
    <div className={classes.marginLeft}>
      
      <Typography component="h1" variant="h5">
        <h3>Search what you want to buy here: car, motorcycle, ...</h3>
      </Typography>
     
      <form onSubmit={(e) => handleSearch(e)} className={classes.form} Validate>
        <TextField variant="outlined" margin="normal" required fullWidth id="search" label="Search Item name"
          name="search" onChange={(e) => setName(e.target.value)} />
        
        <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit} >
          Search
        </Button>
        
      </form>
    </div>
    
  </Container>
  <h1 style={{marginLeft: 200}}>  <ReturnSaleItems saleItems={saleItems}/> </h1>

  </div>

  );
}

export default Home;


