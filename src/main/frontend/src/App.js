import React from 'react';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";

import Navbar from './components/Navbar'
import Home from './components/Home';
import About from './components/About';
import Login from './components/Login';
import Promotion from './components/Promotion';
import './App.css';
import Contact from './components/Contact'
import SignUp from './components/Signup';
import LoginContext from './contexts/LoginContext';


function App() {
  return (
    <Router>
      <Navbar />
      <LoginContext.Provider value={localStorage.getItem("loggedIn")}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/promotion" component={Promotion} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/signup" component={SignUp} />
          {/* <Route exact path="/login" component={Login} /> */}
          {/* <Route exac path="logout" component={Logout} /> */}
        </Switch>
      </LoginContext.Provider>
    </Router>
  );
}

export default App;
