package com.marketplace.webmarket.controller;

import com.marketplace.webmarket.dto.CustomerDTO;
import com.marketplace.webmarket.entity.Customer;
import com.marketplace.webmarket.entity.SaleItem;
import com.marketplace.webmarket.repository.CustomerRepository;
import com.marketplace.webmarket.repository.SaleItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private SaleItemRepository saleItemRepository;

    @PostMapping("/register")
    public CustomerDTO registerCustomer(@RequestBody Customer customer) {
        Customer customer1 = new Customer(
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPassword(),
                customer.getPhoneNumber());

        return customerRepository.save(customer1).getDTO();
    }

    @GetMapping("/")
    public List<CustomerDTO> getAllCustomer()  {

        return customerRepository.findAll()
                .stream()
                .map(Customer::getDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public CustomerDTO getCustomer(@PathVariable("id") int id) {
        return customerRepository.getById(id).getDTO();
    }

    @PutMapping("/{id}")
    public CustomerDTO updateCustomer(@PathVariable("id") int id, @RequestBody CustomerDTO customerDTO) {
        Customer updatedCustomer = customerRepository.getById(id);
        updatedCustomer.setFirstName(customerDTO.getFirstName());
        updatedCustomer.setLastName(customerDTO.getLastName());
        updatedCustomer.setEmail(customerDTO.getEmail());
     //   updatedCustomer.setSaleItems(customerDTO.getSaleItems());

        return customerRepository.save(updatedCustomer).getDTO();

    }

    @PostMapping("/{customer_id}")
    public CustomerDTO updateSaleItem(@PathVariable("customer_id") int customer_id, @RequestBody SaleItem saleItem) {
        Customer customer = customerRepository.getById(customer_id);
        List<SaleItem> updatedSaleItems = customer.getSaleItems();
        updatedSaleItems.add(saleItem);
        saleItemRepository.save(saleItem);
        customer.setSaleItems(updatedSaleItems);
        customerRepository.save(customer);

        return customerRepository.save(customer).getDTO();

    }

    @GetMapping("/{email}/{password}")
    public CustomerDTO getByEmailPassword(@PathVariable String email, @PathVariable String password) {
        return customerRepository.getCustomerByEmailAndPassword(email, password).getDTO();

    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable("id") int id) {
        customerRepository.deleteById(id);
    }
}
