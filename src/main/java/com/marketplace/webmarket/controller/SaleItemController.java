package com.marketplace.webmarket.controller;

import com.marketplace.webmarket.dto.SaleItemDTO;
import com.marketplace.webmarket.entity.Customer;
import com.marketplace.webmarket.entity.SaleItem;
import com.marketplace.webmarket.repository.CustomerRepository;
import com.marketplace.webmarket.repository.SaleItemRepository;
import com.marketplace.webmarket.service.SaleItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/promotions")
public class SaleItemController {

    @Autowired
    SaleItemService saleItemService;
    @Autowired
    SaleItemRepository saleItemRepository;
    @Autowired
    CustomerRepository customerRepository;

    @PostMapping("/{customer_id}")
    public SaleItem createSaleItem(@PathVariable("customer_id") int customer_id, @RequestBody SaleItem saleItem) {
        Customer customer = customerRepository.getById(customer_id);
        List<SaleItem> updatedSaleItems = customer.getSaleItems();
        updatedSaleItems.add(saleItem);
        saleItemRepository.save(saleItem);
        customer.setSaleItems(updatedSaleItems);
        customerRepository.save(customer);

        return  saleItem;//"You have successfully posted you product!";

    }

    @GetMapping("/")
    public List<SaleItem> getAllSaleItems() {

        return saleItemRepository.findAll();

    }

    @GetMapping("/{customer_id}")
    public List<SaleItem> getSaleItemsOfCustomer(@PathVariable("customer_id") int customer_id) {

        Customer customer = new Customer();
        if (customerRepository.findById(customer_id).isPresent()) {
            customer = customerRepository.findById(customer_id).get();
        }
        return customer.getSaleItems();
    }

    @GetMapping("/search/{name}")
    public List<SaleItem> getSaleItemByName(@PathVariable("name") String name) {
        return saleItemRepository.getSaleItemsByItemName(name);
    }


    @PutMapping("/{saleitem_id}")
    public SaleItem updateSaleItem(@PathVariable("saleitem_id") int saleitem_id, @RequestBody SaleItem saleItemDTO) {

        SaleItem saleItem = saleItemRepository.getById(saleitem_id);
        saleItem.setItemName(saleItemDTO.getItemName());
        saleItem.setCity(saleItemDTO.getCity());
        saleItem.setPrice(saleItemDTO.getPrice());
        saleItem.setDescription(saleItemDTO.getDescription());

        return saleItemRepository.save(saleItem);
    }

    @DeleteMapping("/{saleitem_id}")
    public void deleteSaleItem(@PathVariable("saleitem_id")  int saleitem_id) {
        saleItemRepository.deleteById(saleitem_id);
    }

    @GetMapping("/customerId/{id}")
    public String getCIdFromSIId(@PathVariable("id") int id) {
        return saleItemService.getCId(id);
    }

}
