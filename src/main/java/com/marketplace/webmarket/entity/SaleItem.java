package com.marketplace.webmarket.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "sale_items")
public class SaleItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String itemName;
    private String city;
    private double price;
    private String description;


}