package com.marketplace.webmarket.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.marketplace.webmarket.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIgnoreProperties(value = {"handler", "hibernateLazyInitializer", "FieldHandler"})
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;

   @OneToMany//(mappedBy = "customer", cascade = CascadeType.ALL, targetEntity = SaleItem.class)
   @JoinColumn(name = "customer_id")
   List<SaleItem> saleItems;

    public Customer(String firstName, String lastName, String email, String phoneNumber,String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.saleItems = new ArrayList<>();
    }

    public CustomerDTO getDTO() {
        return new CustomerDTO(id, firstName,lastName,email, phoneNumber, saleItems);
    }
}
