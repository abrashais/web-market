package com.marketplace.webmarket.dto;

import com.marketplace.webmarket.entity.SaleItem;
import lombok.Value;

import java.util.List;

@Value
public class CustomerDTO {

    int id;
    String firstName;
    String lastName;
    String email;
    String phoneNumber;
    List<SaleItem> saleItems;

}
