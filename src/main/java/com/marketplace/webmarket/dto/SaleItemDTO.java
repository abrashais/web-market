package com.marketplace.webmarket.dto;

import lombok.Value;

@Value
public class SaleItemDTO {
    int id;
    String itemName;
    String city;
    Double price;
    String description;


}
