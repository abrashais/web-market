package com.marketplace.webmarket.repository;


import com.marketplace.webmarket.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer getCustomerByEmailAndPassword(String email, String password);

}
