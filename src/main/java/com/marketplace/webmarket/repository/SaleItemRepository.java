package com.marketplace.webmarket.repository;

import com.marketplace.webmarket.entity.SaleItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleItemRepository extends JpaRepository<SaleItem, Integer> {
    List<SaleItem> getSaleItemsByItemName(String name);

    
}
