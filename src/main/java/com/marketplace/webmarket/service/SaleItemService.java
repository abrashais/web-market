package com.marketplace.webmarket.service;

import com.marketplace.webmarket.entity.Customer;
import com.marketplace.webmarket.entity.SaleItem;
import com.marketplace.webmarket.repository.CustomerRepository;
import com.marketplace.webmarket.repository.SaleItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SaleItemService {

    @Autowired
    SaleItemRepository saleItemRepository;
    @Autowired
    CustomerRepository customerRepository;

    public String  getCId(int id) {

        SaleItem saleItem = saleItemRepository.getById(id);
        List<Customer> customers = customerRepository.findAll()
                .stream()
                .filter(customer -> customer.getSaleItems().contains(saleItem))
                .collect(Collectors.toList());
        Customer customer = new Customer();
        if (!customers.isEmpty()) {
            customer = customers.get(0);
        }
        return customer.getPhoneNumber();

    }
}

/*(name) => (getCustomerId(name))*/